package main

import (
	"bitbucket/julienpetit/awesome-places-go-api/api"
	"log"
	"net/http"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"bitbucket/julienpetit/awesome-places-go-api/database"
)

var (
	port     string
	user     string
	password string
	server   string
	dbname   string
)

func main() {

	// Load config path
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	// Read config files
	err := viper.ReadInConfig() // Find and read the config file
	api.CheckErr(err, "Fatal error config file")

	server = viper.GetString("database_server")
	user = viper.GetString("user")
	dbname = viper.GetString("dbname")
	password = viper.GetString("password")
	port = viper.GetString("port")

	// Load Database configuration
	conn := user + ":" + password + "@tcp(" + server + ":" + port + ")/" + dbname + "?charset=utf8"

	// Open data connection
	database.Conn, err = gorm.Open("mysql", conn)
	database.Conn.LogMode(true)

	api.CheckErr(err, "sql.Open failed")
	log.Println("Database Connected")

	// db.AutoMigrate(&models.Profile{}, &models.Achievement{})
	log.Println("Model Migration Made")

	//handler := api.NewRouteHandler(*db)
	//router := api.NewRouter(handler)
	log.Println("API ready!")

	router := api.NewRouter()

	log.Fatal(http.ListenAndServe(":8080", router))
}
