package database

import "github.com/jinzhu/gorm"

var (
	// db is the connection handle
	// for the database
	Conn       *gorm.DB
)