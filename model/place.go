package model

type Place struct {
	Id     	   int       `json:"id"`
	Title      string    `json:"title"`
	TitleFr    string    `json:"title_fr"`
	TitleEn    string    `json:"title_en"`
	Lat        float32   `json:"lat"`
	Lon       float32   `json:"lon"`
	Stars      float32   `json:"stars"`
}

type Places []Place
