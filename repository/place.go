package repository

import (
	. "bitbucket/julienpetit/awesome-places-go-api/model"
	_ "github.com/mattn/go-sqlite3"
	"bitbucket/julienpetit/awesome-places-go-api/database"
)

// Give us some seed data
func init() {
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func FindPlaces(lat string, lon string) []Place {

	var places []Place

	// query
	database.Conn.Limit(100).Where("(111.1111 * DEGREES(ACOS(COS(RADIANS(lat)) * COS(RADIANS(?)) * COS(RADIANS(lon - ?)) + SIN(RADIANS(lat)) * SIN(RADIANS(?))))) < 3 ", lat, lon, lat ).Find(&places)

	// return empty Todo if not found
	return places
}


func FindPlace(placeId int) Place {

	return Place{}
}