package api

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},

	Route{
		"PlaceIndex",
		"GET",
		"/places",
		PlaceIndex,
	},
	Route{
		"PlaceShow",
		"GET",
		"/places/{placeId}",
		PlaceShow,
	},
}
