package api

import (
	. "bitbucket/julienpetit/awesome-places-go-api/repository"
	. "bitbucket/julienpetit/awesome-places-go-api/model"
	"github.com/gorilla/mux"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n")
}

func PlaceIndex(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	fmt.Printf("%v", vars)

	var lat string
	var lon string

	lat = r.URL.Query().Get("lat")
	lon = r.URL.Query().Get("long")

	var places []Place
	places = FindPlaces(lat, lon)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(places); err != nil {
		panic(err)
	}
}

func PlaceShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var placeId int
	var err error

	if placeId, err = strconv.Atoi(vars["placeId"]); err != nil {
		panic(err)
	}

	place := FindPlace(placeId)
	if place.Id > 0 {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(place); err != nil {
			panic(err)
		}
		return
	}

	// If we didn't find it, 404
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusNotFound)
	if err := json.NewEncoder(w).Encode(jsonErr{Code: http.StatusNotFound, Text: "Not Found"}); err != nil {
		panic(err)
	}
}