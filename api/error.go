package api

import "log"

type jsonErr struct {
	Code int    `json:"code"`
	Text string `json:"text"`
}

func CheckErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}